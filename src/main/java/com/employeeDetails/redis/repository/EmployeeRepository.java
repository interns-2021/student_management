package com.employeeDetails.redis.repository;


	
	import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeDetails.redis.modol.Employee;

	@Repository
	public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
	}


