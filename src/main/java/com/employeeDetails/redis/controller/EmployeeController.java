package com.employeeDetails.redis.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.employeeDetails.redis.ResourceNotFoundException;
import com.employeeDetails.redis.modol.Employee;
import com.employeeDetails.redis.repository.EmployeeRepository;




import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	
	 @Autowired
	    private EmployeeRepository employeeRepository;

	 
	
		
	    @PostMapping("/saveEmployees")
	    public Employee addEmployee(@RequestBody Employee employee) {

	        return employeeRepository.save(employee);
	    }


	    @GetMapping("/employees")
	    public ResponseEntity<List<Employee>> getAllEmployees() {
	        return ResponseEntity.ok(employeeRepository.findAll());
	    }

	    @GetMapping("employees/{emp_id}")
	    @Cacheable(value = "employees",key = "#emp_id")
	    public Employee findEmployeeById(@PathVariable(value = "emp_id") Integer emp_id) {
	        System.out.println("Employee fetching from database:: "+emp_id);
	        return employeeRepository.findById(emp_id).orElseThrow(
	                () -> new ResourceNotFoundException("Employee not found" + emp_id));

	    }


	    @PutMapping("updateEmployees/{emp_id}")
	    @CachePut(value = "employees",key = "#emp_id")
	    public Employee updateEmployee(@PathVariable(value = "emp_id") Integer emp_id,
	                                                   @RequestBody Employee employeeDetails) {
	        Employee employee = employeeRepository.findById(emp_id)
	                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + emp_id));
	        employee.setEmp_Name(employeeDetails.getEmp_Name());
	        final Employee updatedEmployee = employeeRepository.save(employee);
	        return updatedEmployee;

	    }


	    @DeleteMapping("deleteEmployees/{emp_id}")
	    @CacheEvict(value = "employees", allEntries = true)
	    public void deleteEmployee(@PathVariable(value = "emp_id") Integer emp_id) {
	        Employee employee = employeeRepository.findById(emp_id).orElseThrow(
	                () -> new ResourceNotFoundException("Employee not found" + emp_id));
	        employeeRepository.delete(employee);
	    }
	    
		

}
